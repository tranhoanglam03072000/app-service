/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {
      gridRow: {
        "span-8": "span 8 / span 8",
        "span-7": "span 7 / span 7",
      },
      gridTemplateRows: {
        // Simple 8 row grid
        8: "repeat(8, minmax(0, 1fr))",
      },
      height: {
        "1/8": "12.5%",
        "1/10": "10%",
        "7/60": "11.6%",
        "16/60": "29.6%",
        "15/60": "25%",
        "22/60": "33.6%",
      },
      width: {
        "5/13": "37%",
        "3/13": "23.07%",
        "6/13": "46.14%",
      },
      gridTemplateColumns: {
        // Simple 16 column grid
        36: "repeat(36, minmax(0, 1fr))",
      },
      gridColumn: {
        "span-17": "span 17 / span 17",
        "span-27": "span 27 / span 27",
      },
    },
  },
  plugins: [],
};
