export const HomePageLightTheme = {
  bgMenuHover: "#D1D5DA",
  textColorMenuSelectd: "#2984ff",
  bgMenuHomePage: "white",
  colorLogo: "black",
  textMenu: "black",
  bgHelpRight: "white",
  bgButtonChangTheme: "white",
  textButtonChangeTheme: "black",
  // NAV BAR
  bgNavBarHomePage: "white",
  bgNavBarSearch: "white",
  textSearchInput: "black",
  textSearchPlaceHolder: "#9BA3AF",
  bgButtonCreate: "#2984ff",
  colorBorderButtonCreate: "#9BA3AF",
  colorIconNavBar: "black",
  bgCommand: "#6A7280",
  //List User
  bgItemUserHover: "#D1D5DA",
  colorTextNameItemUser: "black",
  colorTextEmailItemUser: "#9BA3AF",
  //textediter
  bgToolBarTextEditer: "#D1D5DA",
  colorIconTextEditer: "black",
  bgBodyTextEditerContent: "white",
};
