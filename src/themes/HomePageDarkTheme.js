export const HomePageDarkTheme = {
  bgMenuHover: "#272a30",
  textColorMenuSelectd: "white",
  bgMenuHomePage: "#1a1c1e",
  colorLogo: "white",
  textMenu: "#79808a",
  bgHelpRight: "#d8b3fe",
  bgButtonChangTheme: "#0e0e0e",
  textButtonChangeTheme: "white",
  // Nav Bar
  bgNavBarHomePage: "#1a1c1e",
  bgNavBarSearch: "#272a30",
  textSearchInput: "white",
  textSearchPlaceHolder: "#464c51",
  bgButtonCreate: "#2984ff",
  colorBorderButtonCreate: "#2984ff",
  colorIconNavBar: "#464c51",
  bgCommand: "black",
  //List User
  bgItemUserHover: "#272a30",
  colorTextNameItemUser: "white",
  colorTextEmailItemUser: "#9BA3AF",
  //textediter
  bgToolBarTextEditer: "#272A30",
  colorIconTextEditer: "#9BA3AF",
  bgBodyTextEditerContent: "#111315",
};
