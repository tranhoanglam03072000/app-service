import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
const firebaseConfig = {
  apiKey: "AIzaSyBAVMzxbP0W4gzCyj4uK0vX2fBT6Y4lyx0",
  authDomain: "app-service-c1d9d.firebaseapp.com",
  projectId: "app-service-c1d9d",
  storageBucket: "app-service-c1d9d.appspot.com",
  messagingSenderId: "384710604149",
  appId: "1:384710604149:web:86554723290f37f0db8f3c",
  measurementId: "G-HFKX1ZRDCY",
};
const app = initializeApp(firebaseConfig);

export const auth = getAuth(app);
