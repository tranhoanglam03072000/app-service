const regex = {
  email:
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
  password: /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/,
};
export const checkValidation = (name, value) => {
  if (value < 1) {
    let textErr = value === "" ? "* Không được để trống" : " ";
    return textErr;
  } else {
    switch (name) {
      case "email":
        {
          const textErr = regex.email.test(value)
            ? ""
            : "* Email chưa đúng định dạng";
          return textErr;
        }
        break;
      case "password": {
        const textErr = regex.password.test(value)
          ? ""
          : "* Tối thiểu tám ký tự, ít nhất một chữ cái và một số, không chứa kí tự đặc biệt";
        return textErr;
      }
    }
  }
};

export const submitFormValidation = (dataValue, dataErr) => {
  let isValid = true;
  for (let key in dataValue) {
    if (dataErr[key] !== "" || dataValue[key] === "") {
      isValid = false;
    }
  }
  return isValid;
};
