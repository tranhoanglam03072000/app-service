import { message } from "antd";
import { signInWithEmailAndPassword } from "firebase/auth";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import * as customs from "../../components/HomePage/customs";
import { auth } from "../../firebas-config";
import { checkValidation, submitFormValidation } from "./LoginForm.utils";

export default function Login({ signinIn }) {
  let dispatch = useDispatch();
  let navigate = useNavigate();
  let [valueInput, setValueInput] = useState({
    email: "",
    password: "",
  });
  let [errValueInput, setErrValueInput] = useState({
    email: "",
    password: "",
  });
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);
  let handleChangeValueInput = (e) => {
    let { value, name } = e.target;
    setErrValueInput({
      ...errValueInput,
      [name]: checkValidation(name, value),
    });
    setValueInput({ ...valueInput, [name]: value });
  };
  // Submit-------------------------
  let handleSubmit = (e) => {
    e.preventDefault();
    const isValid = submitFormValidation(valueInput, errValueInput);
    // Send request data register
    if (isValid) {
      const register = async () => {
        try {
          const user = await signInWithEmailAndPassword(
            auth,
            valueInput.email,
            valueInput.password
          );
          console.log("user", user);
          message.success(`Login success! hello ${user.user.email}`);
          navigate("/home");
        } catch (err) {
          message.error(err.message);
        }
      };
      register();
    }
  };
  return (
    <customs.SignInContainer signinIn={signinIn}>
      <customs.Form>
        <customs.Title>Sign in</customs.Title>
        <div className="w-full mb-2">
          <customs.Input
            name="email"
            label="email"
            placeholder="Email"
            value={valueInput.email}
            valueErr={errValueInput.email}
            onChange={handleChangeValueInput}
          />
          <div className="h-5 text-red-500 text-left">
            {errValueInput.email}
          </div>
        </div>

        <div className="w-full mb-2">
          <customs.Input
            type="password"
            name="password"
            placeholder="Password"
            value={valueInput.password}
            valueErr={errValueInput.password}
            onChange={handleChangeValueInput}
          />
          <div className="h-10 text-red-500 text-left">
            {errValueInput.password}
          </div>
        </div>

        <customs.Button type="submit" onClick={handleSubmit}>
          Sigin In
        </customs.Button>
      </customs.Form>
    </customs.SignInContainer>
  );
}
