import React, { useState } from "react";
import * as customs from "../../components/HomePage/customs";
import Login from "./Login";
// import { postSignUpAsync } from "../../redux/userSlice";
import { useDispatch } from "react-redux";
import { checkValidation, submitFormValidation } from "./LoginForm.utils";
import { auth } from "../../firebas-config";
import { createUserWithEmailAndPassword } from "firebase/auth";
import { message } from "antd";

export default function LoginAndRegister() {
  let dispatch = useDispatch();
  const [signIn, toggle] = useState(true);
  let [dataRegister, setdataRegister] = useState({
    email: "",
    password: "",
  });
  let [dataRegisterErr, setdataRegisterErr] = useState({
    email: "",
    password: "",
  });

  let handleChangeValueInput = (e) => {
    let { value, name } = e.target;
    //Validation
    setdataRegisterErr({
      ...dataRegisterErr,
      [name]: checkValidation(name, value),
    });
    // change value
    setdataRegister({ ...dataRegister, [name]: value });
  };
  let handleSubmit = (e) => {
    e.preventDefault();
    const isValid = submitFormValidation(dataRegister, dataRegisterErr);
    // Send request data register
    if (isValid) {
      const register = async () => {
        try {
          const user = await createUserWithEmailAndPassword(
            auth,
            dataRegister.email,
            dataRegister.password
          );
          message.success("Register success");
        } catch (err) {
          message.error(err.message);
        }
      };
      register();
    }
  };
  return (
    <div className="flex justify-center items-center h-screen w-screen">
      <customs.Container>
        {/*\SIGN UP */}
        <customs.SignUpContainer signinIn={signIn}>
          <customs.Form method="get">
            <customs.Title>Sign Up</customs.Title>
            <div className="w-full mb-2">
              <customs.Input
                name="email"
                placeholder="Email"
                value={dataRegister.email}
                onChange={handleChangeValueInput}
              />
              <div className="h-5 text-red-500 text-left">
                {dataRegisterErr.email}
              </div>
            </div>

            <div className="w-full mb-2">
              <customs.Input
                name="password"
                type="password"
                placeholder="Password"
                value={dataRegister.password}
                onChange={handleChangeValueInput}
              />
              <div className="h-10 text-red-500 text-left">
                {dataRegisterErr.password}
              </div>
            </div>
            <customs.Button type="submit" onClick={handleSubmit}>
              Sign Up
            </customs.Button>
          </customs.Form>
        </customs.SignUpContainer>
        {/*  */}
        {/*  */}
        {/*  */}
        {/*  */}
        {/*  */}
        {/*  */}
        {/*  */}
        {/* Login */}
        <Login signinIn={signIn} />
        {/*overlay Login to Sigup and != */}
        <customs.OverlayContainer signinIn={signIn}>
          <customs.Overlay signinIn={signIn}>
            <customs.LeftOverlayPanel signinIn={signIn}>
              <customs.Title>Welcome Back!</customs.Title>
              <customs.Paragraph>
                To keep connected with us please login with your personal info
              </customs.Paragraph>
              <customs.GhostButton onClick={() => toggle(true)}>
                Sign In
              </customs.GhostButton>
            </customs.LeftOverlayPanel>

            <customs.RightOverlayPanel signinIn={signIn}>
              <customs.Title>Hello, Friend!</customs.Title>
              <customs.Paragraph>
                Enter Your personal details and start journey with us
              </customs.Paragraph>
              <customs.GhostButton onClick={() => toggle(false)}>
                Sigin Up
              </customs.GhostButton>
            </customs.RightOverlayPanel>
          </customs.Overlay>
        </customs.OverlayContainer>
      </customs.Container>
    </div>
  );
}
