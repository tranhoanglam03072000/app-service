import React, { useState } from "react";

import { items, rootSubmenuKeys } from "./MenuHome.utils";
import { QuestionCircleOutlined } from "@ant-design/icons";
import {
  ButtonChangeTheme,
  LogoMenuCusTom,
  MenuBottom,
  MenuCustom,
  MenuHomePageCustom,
} from "../../../components/HomePage/MenuHome";
import { ThemeProvider } from "styled-components";

import { useDispatch, useSelector } from "react-redux";
import { changeThemeAction } from "../../../redux/ThemeReducer/themeReducer";

export default function MenuHome() {
  const [openKeys, setOpenKeys] = useState(["sub1"]);
  const dispatch = useDispatch();
  const onOpenChange = (keys) => {
    const latestOpenKey = keys.find((key) => openKeys.indexOf(key) === -1);
    if (rootSubmenuKeys.indexOf(latestOpenKey) === -1) {
      setOpenKeys(keys);
    } else {
      setOpenKeys(latestOpenKey ? [latestOpenKey] : []);
    }
  };
  const { theme, active } = useSelector((state) => {
    return state.themeReducer;
  });
  const handleChangeTheme = () => {
    dispatch(changeThemeAction(!active));
  };

  return (
    <ThemeProvider theme={theme}>
      <MenuHomePageCustom className="menu-home-page">
        <LogoMenuCusTom className="logo">
          <i className="fab fa-github-alt"></i>
        </LogoMenuCusTom>
        {/* //Menu List  */}
        <div className="menu-list">
          <MenuCustom
            itemType="SubMenuType"
            mode="inline"
            openKeys={openKeys}
            onOpenChange={onOpenChange}
            className={`space-y-3`}
            style={{
              width: "100%",
              height: "100%",
              borderBottom: "1px gray solid",
              transition: "0s",
            }}
            items={items}
          />
        </div>
        {/* // Bot  */}
        <MenuBottom className="menu-bottom">
          <div className="help">
            <div className="help-left">
              <QuestionCircleOutlined />
              <span className="text-xs">Help & getting started</span>
            </div>
            <span className="help-right">8 </span>
          </div>
          {/* // CHANGE THEME  */}
          <ButtonChangeTheme
            onClick={() => {
              handleChangeTheme();
            }}
            className="switch-theme"
          >
            <span className="style-theme">
              <i className="fa fa-sun"></i> <span>Light</span>
            </span>
            <span className="style-theme">
              <i className="fa fa-moon"></i> <span>Dark</span>
            </span>
            <div className={active ? "light-theme theme" : "dark-theme theme"}>
              {active ? (
                <div className="style-theme">
                  <i className="fa fa-sun"></i> <span>Light</span>
                </div>
              ) : (
                <div>
                  <i className="fa fa-moon"></i> <span>Dark</span>
                </div>
              )}
            </div>
          </ButtonChangeTheme>
        </MenuBottom>
      </MenuHomePageCustom>
    </ThemeProvider>
  );
}
