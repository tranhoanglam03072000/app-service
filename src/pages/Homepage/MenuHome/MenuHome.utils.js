import {
  ShopOutlined,
  SketchOutlined,
  HomeOutlined,
  UserOutlined,
  RiseOutlined,
  PieChartOutlined,
} from "@ant-design/icons";
export function getItem(label, key, icon, children, type) {
  return {
    key,
    icon,
    children,
    label,
    type,
  };
}
export const items = [
  getItem("Home", "sub1", <HomeOutlined />),
  getItem("Product", "sub2", <SketchOutlined />, [
    getItem(<div className="tree">Product 1</div>, "1"),
    getItem(<div className="tree-end">Product 2</div>, "2"),
  ]),
  getItem("Customers", "sub3", <UserOutlined />, [
    getItem(<div className="tree">Overview</div>, "3"),
    getItem(<div className="tree-end">Customer list</div>, "4"),
  ]),
  getItem("Shop", "sub4", <ShopOutlined />),
  getItem("Income", "sub5", <PieChartOutlined />, [
    getItem(<div className="tree">Income 1</div>, "5"),
    getItem(<div className="tree-end">Income 2</div>, "6"),
  ]),
  getItem("Promote", "sub6", <RiseOutlined />),
];
export const rootSubmenuKeys = ["sub1", "sub2", "sub3", "sub4", "sub5", "sub6"];
