import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { RightOutlined } from "@ant-design/icons";
import { changeUserActiveAction } from "../../../redux/UserManagementReducer/userManagementReducer";
import {
  ItemUser,
  ItemUserActive,
  ListUserCustom,
  TextEmailItemUser,
  TextNameItemUser,
  TitleListUser,
} from "../../../components/HomePage/ListUser";
import { ThemeProvider } from "styled-components";
export default function ListUser() {
  const { listUser } = useSelector((state) => state.userManagementReducer);
  const { theme } = useSelector((state) => state.themeReducer);
  const dispatch = useDispatch();
  const handleChangeUserActive = (id) => {
    dispatch(changeUserActiveAction(id));
  };
  const renderItemUser = () => {
    return listUser?.map((itemUser) => {
      if (!itemUser.active) {
        return (
          <ItemUser
            onClick={() => {
              handleChangeUserActive(itemUser.id);
            }}
            key={itemUser.id}
            className="item-user"
          >
            <div className="item-user-left">
              <div className="box"></div>
            </div>
            <div className="item-user-right ">
              <img
                src={itemUser.img}
                className="w-10 h-10 rounded-full object-cover"
                alt=""
              />
              <div className="w-3/4">
                <TextNameItemUser>{itemUser.name}</TextNameItemUser>
                <div className="text-gray-400">{itemUser.email}</div>
              </div>
            </div>
          </ItemUser>
        );
      } else {
        return (
          <ItemUserActive
            key={itemUser.id}
            className="item-user relative  rounded-xl"
          >
            <div className="item-user-left">
              <div className="box bg-blue-400 text-white text-center">
                <i className="fa fa-check"></i>
              </div>
            </div>
            <div className="item-user-right ">
              <img
                src={itemUser.img}
                className="w-10 h-10 rounded-full object-cover"
                alt=""
              />
              <div className="w-3/4">
                <TextNameItemUser className="text-white">
                  {itemUser.name}
                </TextNameItemUser>
                <TextEmailItemUser>{itemUser.email}</TextEmailItemUser>
                <div className="absolute font-extrabold text-gray-400 top-1/3 right-2">
                  <RightOutlined />
                </div>
              </div>
            </div>
          </ItemUserActive>
        );
      }
    });
  };
  return (
    <ThemeProvider theme={theme}>
      <ListUserCustom className="list-user ">
        <div className="w-full h-full">
          {/* // title  */}
          <TitleListUser className="flex w-full h-[7%]  border-gray-800 items-center py-2">
            <div className="w-1/6 h-full px-2">
              <div className="border-gray-600 rounded border-2 h-5 w-5"></div>
            </div>
            <TextNameItemUser className="w-5/6 flex items-center space-x-2 h-full ">
              Name
            </TextNameItemUser>
          </TitleListUser>
          {/* Render */}
          <div className="h-[93%] overflow-scroll">{renderItemUser()}</div>
        </div>
      </ListUserCustom>
    </ThemeProvider>
  );
}
