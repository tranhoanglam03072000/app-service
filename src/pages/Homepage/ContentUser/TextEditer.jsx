import React, { useEffect, useRef } from "react";
import { Editor } from "@tinymce/tinymce-react";
import { TextEditerCustom } from "../../../components/HomePage/ContentUser";
import { ThemeProvider } from "styled-components";
import { useSelector } from "react-redux";

export default function TextEditer() {
  const editorRef = useRef(null);
  const { active, theme } = useSelector((state) => state.themeReducer);
  useEffect(() => {
    // window.location.reload();
    console.log(1);
  });
  return (
    <ThemeProvider theme={theme}>
      <TextEditerCustom className="h-5/6 w-full">
        {/* //Dark and light  */}
        <Editor
          apiKey="lngm3au1x3ilu86kc62t1tsh8jr8fb4mzpm2lyvc5kr5t8mr"
          onInit={(evt, editor) => (editorRef.current = editor)}
          initialValue=""
          init={{
            height: "100%",
            width: "100%",
            menubar: false,
            statusbar: false,
            selector: "textarea",
            plugins:
              "anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount checklist mediaembed casechange export formatpainter pageembed linkchecker a11ychecker tinymcespellchecker permanentpen powerpaste advtable advcode editimage tinycomments tableofcontents footnotes mergetags autocorrect typography inlinecss importcss",
            toolbar:
              "bold italic underline emoticons link bullist align | undo redo",
            tinycomments_mode: "embedded",
            tinycomments_author: "Author name",
            mergetags_list: [
              { value: "First.Name", title: "First Name" },
              { value: "Email", title: "Email" },
            ],
            content_style: `#tinymce {color : gray}`,
          }}
        />
      </TextEditerCustom>
    </ThemeProvider>
  );
}
//
