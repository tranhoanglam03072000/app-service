import React from "react";
import { useSelector } from "react-redux";
import { ThemeProvider } from "styled-components";
import {
  ContentUserCusTom,
  HeaderUserContent,
  ItemProduct,
  TextMail,
  TextName,
} from "../../../components/HomePage/ContentUser";
import {
  MessageOutlined,
  AliyunOutlined,
  TwitterOutlined,
  InstagramOutlined,
  MailOutlined,
  InfoCircleFilled,
} from "@ant-design/icons";
import TextEditer from "./TextEditer";
const dataProduct = [
  {
    id: 1,
    img: "https://bizweb.dktcdn.net/100/413/756/products/dm8947-100-01-1657444597796.png?v=1657444604197",
    name: "Product title",
    link: "ui8.net/product/product-link",
    price: "$64.00",
    date: "Apr 9,2021",
    bgPrice: "#B4E5CA",
  },
  {
    id: 2,
    img: "https://media.gq.com/photos/5ad64204c8be07604e8b5f2f/3:2/w_1998,h_1332,c_limit/21-books-GQ-April-2018-041718-3x2.jpg",
    name: "Product title",
    link: "ui8.net/product/product-link",
    price: "$64.00",
    date: "Apr 9,2021",
    bgPrice: "#B6E4CD",
  },
  {
    id: 3,
    img: "https://cf.shopee.vn/file/fb3b62476982930520a5252db0f375ba",
    name: "Product title",
    link: "ui8.net/product/product-link",
    price: "$64.00",
    date: "Apr 9,2021",
    bgPrice: "#FFD88C",
  },
];
export default function ContentUser() {
  const { userActive } = useSelector((state) => {
    return state.userManagementReducer;
  });
  const { theme } = useSelector((state) => {
    return state.themeReducer;
  });
  const renderProduct = () => {
    return dataProduct.map((item) => {
      return (
        <ItemProduct className="product-item" key={item.id}>
          <div className="w-3/5 flex items-center text space-x-3">
            <img src={item.img} alt="" />
            <div className="w-3/4">
              <TextName className="text-name">{item.name}</TextName>
              <div className="text-link">{item.link}</div>
            </div>
          </div>
          <div className="w-1/5">
            <span
              style={{
                background: `${item.bgPrice}`,
              }}
              className={` px-2 py-[6px] rounded font-bold text-xs`}
            >
              {item.price}
            </span>
          </div>
          <div className="w-1/5 text-gray-400 text-xs">{item.date}</div>
        </ItemProduct>
      );
    });
  };
  console.log("userActive: ", userActive);
  return (
    <ThemeProvider theme={theme}>
      <ContentUserCusTom className="content-user ">
        <div className=" h-full w-full">
          {/* //Header */}
          <HeaderUserContent className="header-user-content ">
            <div className="header-user-content-left">
              <div className="w-16 h-16 rounded-full border border-gray-500 overflow-hidden">
                <img
                  src={userActive.img}
                  alt=""
                  className="w-full h-full object-cover"
                />
              </div>
              <div className="w-3/4 space-y-1">
                <TextName className="text-name">{userActive.name}</TextName>
                <TextMail className="text-mail">{userActive.email}</TextMail>
              </div>
            </div>
            <div className="header-user-content-right flex w-1/2 items-center justify-end space-x-3">
              <div className="button-header-content">
                <TextName>Follow</TextName>
                <TextName>
                  <i className="fa fa-plus"></i>
                </TextName>
              </div>
              <div className="button-header-content">
                <TextName>Message</TextName>
                <TextName className="flex items-center">
                  <MessageOutlined />
                </TextName>
              </div>
            </div>
          </HeaderUserContent>
          {/* //Textedittor */}
          <div className="text-editor  h-16/60 pt-2 space-y-1">
            <div className="flex items-center text-xs h-1/6 space-x-2">
              <TextName>Private note</TextName>
              <TextMail className="flex items-center">
                <InfoCircleFilled />
              </TextMail>
            </div>
            <TextEditer />
          </div>
          {/* //contact */}
          <div className="contact-user ">
            <div className="contact-mail ">
              <MailOutlined />
              <TextName className="text-xs ">fahey.designer@robot.co</TextName>
            </div>
            {/* // */}
            <div className="contact-social font-extrabold">
              <TwitterOutlined />
              <InstagramOutlined />
              <i className="fab fa-pinterest-p"></i>
              <i className="fab fa-facebook-f"></i>
            </div>
            {/* // */}
            <div className="contact-attachment">
              <AliyunOutlined />
              <TextName className="text-xs ">robot.co</TextName>
            </div>
          </div>
          {/* //Product */}
          <div className="product-user">
            <div className="flex items-center h-1/4 text-gray-400 border-b-[0.5px] border-gray-600 font-bold">
              <span className="w-3/5">Product</span>
              <span className="w-1/5">Price</span>
              <span className="w-1/5">Date</span>
            </div>
            {renderProduct()}
          </div>
        </div>
      </ContentUserCusTom>
    </ThemeProvider>
  );
}
