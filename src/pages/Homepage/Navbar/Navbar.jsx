import React from "react";
import { MessageOutlined, BellOutlined } from "@ant-design/icons";
import {
  ButtonCreate,
  IconNavBar,
  NavBarCustom,
  NavBarRightCustom,
  NavBarSearch,
} from "../../../components/HomePage/NavBar";
import { ThemeProvider } from "styled-components";
import { useSelector } from "react-redux";
export default function Navbar() {
  const { theme } = useSelector((state) => state.themeReducer);
  return (
    <ThemeProvider theme={theme}>
      <NavBarCustom className="nav-bar">
        <div className="flex items-center justify-between h-full">
          <NavBarSearch className="nav-bar-search">
            <div className="search">
              <div className="search-left">
                <i className="fa fa-search text-lg"></i>
                <input
                  type="text"
                  placeholder="Search or type a command"
                  className="w-full h-full"
                />
              </div>
              <span className="search-icon">⌘ F</span>
            </div>
          </NavBarSearch>
          <div className="w-3/13"></div>
          <NavBarRightCustom className=" nav-bar-right">
            <ButtonCreate className="button-create">
              <i className="fa fa-plus"></i>
              <span>Create</span>
            </ButtonCreate>
            <IconNavBar className="message ">
              <MessageOutlined />
              <div className="dot-red"></div>
            </IconNavBar>
            <IconNavBar className="bell ">
              <BellOutlined />
              <div className="dot-red"></div>
            </IconNavBar>
            <div className="avatar w-10 h-10 overflow-hidden rounded-full border-gray-600 border">
              <img src="https://i.pravatar.cc/300" alt="" />
            </div>
          </NavBarRightCustom>
        </div>
      </NavBarCustom>
    </ThemeProvider>
  );
}
