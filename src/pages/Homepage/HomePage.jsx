import React from "react";
import ContentUser from "./ContentUser/ContentUser";
import ListUser from "./ListUser/ListUser";
import MenuHome from "./MenuHome/MenuHome";
import Navbar from "./Navbar/Navbar";

export default function HomePage() {
  return (
    <main style={{ background: "#757C85" }} className="">
      <main className="grid grid-cols-36 grid-rows-8 container mx-auto w-screen h-screen gap-4 py-2">
        <MenuHome />
        <Navbar />
        <ListUser />
        <ContentUser />
      </main>
    </main>
  );
}
