import { Menu } from "antd";
import styled from "styled-components";

export const MenuHomePageCustom = styled.div`
  background: ${(props) => props.theme.bgMenuHomePage};
`;
export const LogoMenuCusTom = styled.div`
  color: ${(props) => props.theme.colorLogo};
`;
export const MenuCustom = styled(Menu)`
  :where(.css-dev-only-do-not-override-k83k30).ant-menu-light {
    color: rgba(0, 0, 0, 0.88);
    background: ${(props) => props.theme.bgMenuHomePage};
    transition: 0s;
  }
  .ant-menu-submenu-title:hover {
    background-color: ${(props) => props.theme.bgMenuHover} !important;
    color: ${(props) => props.theme.textColorMenuSelectd} !important;
  }
  .ant-menu-submenu-selected {
    background-color: ${(props) => props.theme.bgMenuHover} !important;
    transition: 0s;
  }
  .ant-menu-inline {
    background: ${(props) => props.theme.bgMenuHomePage} !important;
    transition: 0s;
    overflow: unset !important;
  }
  .ant-menu-inline .ant-menu-item {
    overflow: unset !important;
  }
  .ant-menu-submenu-open {
    background: ${(props) => props.theme.bgMenuHomePage} !important;
    transition: 0s;
  }
  .ant-menu-submenu-selected .ant-menu-submenu-title {
    color: ${(props) => props.theme.textColorMenuSelectd} !important;
    transition: 0s;
  }
  .ant-menu-item:hover {
    background-color: ${(props) => props.theme.bgMenuHover} !important;
    color: ${(props) => props.theme.textColorMenuSelectd} !important;
  }
  .ant-menu-item-only-child:hover {
    background-color: ${(props) => props.theme.bgMenuHomePage} !important;
  }
  .ant-menu-item-only-child .ant-menu-title-content {
    padding-left: 10px;
    transition: 0s;
    overflow: unset !important;
  }
  .ant-menu-item-only-child .ant-menu-title-content:hover {
    background-color: ${(props) => props.theme.bgMenuHover} !important;
    border-radius: 10px !important;
  }
  .ant-menu-item-selected {
    background-color: ${(props) => props.theme.bgMenuHover} !important;
    color: ${(props) => props.theme.textColorMenuSelectd} !important;
    transition: 0s;
  }
  .ant-menu-sub .ant-menu-item-selected {
    background: ${(props) => props.theme.bgMenuHomePage} !important;
    transition: 0s;
  }
  color: ${(props) => props.theme.textMenu} !important;
`;

export const MenuBottom = styled.div`
  .help-left {
    color: ${(props) => props.theme.textMenu};
  }
  .help-right {
    background-color: ${(props) => props.theme.bgHelpRight};
  }
`;

export const ButtonChangeTheme = styled.div`
  .light-theme {
    background: #272a30;
    color: white;
  }
  .dark-theme {
    background: #272a30;
    color: white;
  }
  background: ${(props) => props.theme.bgButtonChangTheme};
  color: ${(props) => props.theme.textButtonChangeTheme};
`;
