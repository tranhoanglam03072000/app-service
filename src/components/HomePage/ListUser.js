import styled from "styled-components";

export const ListUserCustom = styled.div`
  background: ${(props) => props.theme.bgNavBarHomePage};
`;

export const TitleListUser = styled.div``;
export const ItemUser = styled.div`
  border-color: ${(props) => props.theme.bgItemUserHover};

  &:hover {
    background: ${(props) => props.theme.bgItemUserHover};
  }
`;
export const ItemUserActive = styled.div`
  background: ${(props) => props.theme.bgItemUserHover};
  border-color: ${(props) => props.theme.bgItemUserHover};
`;
export const TextNameItemUser = styled.div`
  color: ${(props) => props.theme.colorTextNameItemUser};
  font-weight: bold;
`;

export const TextEmailItemUser = styled.div`
  color: ${(props) => props.theme.colorTextEmailItemUser};
`;
