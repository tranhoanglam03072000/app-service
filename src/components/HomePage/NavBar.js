import styled from "styled-components";

export const NavBarCustom = styled.div`
  background: ${(props) => props.theme.bgNavBarHomePage};
`;

export const NavBarSearch = styled.div`
  background: ${(props) => props.theme.bgNavBarSearch};
  .search-icon {
    background: ${(props) => props.theme.bgCommand};
  }
  input {
    background: ${(props) => props.theme.bgNavBarSearch};
    color: ${(props) => props.theme.textSearchInput};

    &::placeholder {
      color: ${(props) => props.theme.textSearchPlaceHolder};
    }
  }
`;

export const NavBarRightCustom = styled.div``;
export const ButtonCreate = styled.div`
  background: ${(props) => props.theme.bgButtonCreate};
  color: white;
  border: 1px solid ${(props) => props.theme.colorBorderButtonCreate};
`;
export const IconNavBar = styled.div`
  color: ${(props) => props.theme.colorIconNavBar};
`;
