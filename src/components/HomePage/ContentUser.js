import { Editor } from "@tinymce/tinymce-react";
import styled from "styled-components";

export const ContentUserCusTom = styled.div`
  background: ${(props) => props.theme.bgMenuHomePage};
`;
export const HeaderUserContent = styled.div``;
export const TextName = styled.span`
  color: ${(props) => props.theme.colorTextNameItemUser};
`;
export const TextMail = styled.span`
  color: ${(props) => props.theme.colorTextEmailItemUser};
`;
export const ItemProduct = styled.div`
  &:hover {
    background: ${(props) => props.theme.bgItemUserHover};
  }
`;
export const TextEditerCustom = styled.div`
  .tox:not(.tox-tinymce-inline) .tox-editor-header {
    background: ${(props) => props.theme.bgToolBarTextEditer} !important;
    padding: 0px 5px;
  }
  .tox .tox-toolbar__primary {
    background-color: ${(props) => props.theme.bgToolBarTextEditer} !important;
    display: flex !important;
    justify-content: space-between !important;
  }
  .tox-tinymce {
    border: 1px solid ${(props) => props.theme.bgToolBarTextEditer} !important;
  }
  .tox .tox-tbtn svg {
    fill: ${(props) => props.theme.colorIconTextEditer} !important;
  }

  .tox .tox-edit-area__iframe {
    background: ${(props) => props.theme.bgBodyTextEditerContent} !important;
  }
`;
