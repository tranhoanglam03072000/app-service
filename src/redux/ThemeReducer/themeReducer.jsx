import { createSlice } from "@reduxjs/toolkit";
import { HomePageDarkTheme } from "../../themes/HomePageDarkTheme";
import { HomePageLightTheme } from "../../themes/HomePageLightTheme";

const initialState = {
  theme: HomePageDarkTheme,
  active: false,
  number: 1,
};

const themeReducer = createSlice({
  name: "themeReducer",
  initialState,
  reducers: {
    changeThemeAction: (state, { payload }) => {
      state.active = payload;
      if (payload) {
        state.theme = HomePageLightTheme;
        state.number = 2;
      } else {
        state.theme = HomePageDarkTheme;
        state.number = 1;
      }
    },
  },
});

export const { changeThemeAction } = themeReducer.actions;

export default themeReducer.reducer;
