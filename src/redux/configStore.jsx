import { configureStore } from "@reduxjs/toolkit";
import themeReducer from "./ThemeReducer/themeReducer";
import userManagementReducer from "./UserManagementReducer/userManagementReducer";

export const store = configureStore({
  reducer: {
    userManagementReducer,
    themeReducer,
  },
});
