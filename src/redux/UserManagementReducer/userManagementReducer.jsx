import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  listUser: [
    {
      id: 1,
      name: "Chelsie",
      email: "Chelsie@gmail.com",
      img: "https://i.pravatar.cc/150?img=1",
      active: true,
    },
    {
      id: 2,
      name: "Chelsie Haley",
      email: "ChelsieHaley@gmail.com",
      img: "https://i.pravatar.cc/150?img=2",
      active: false,
    },
    {
      id: 3,
      name: "Orion",
      email: "Orion@gmail.com",
      img: "https://i.pravatar.cc/150?img=3",
      active: false,
    },
    {
      id: 4,
      name: "Brown",
      email: "Brown@gmail.com",
      img: "https://i.pravatar.cc/150?img=4",
      active: false,
    },
    {
      id: 5,
      name: "Bessie",
      email: "Bessie@gmail.com",
      img: "https://i.pravatar.cc/150?img=5",
      active: false,
    },
    {
      id: 6,
      name: "Ellie",
      email: "Ellie@gmail.com",
      img: "https://i.pravatar.cc/150?img=6",
      active: false,
    },
    {
      id: 7,
      name: "Dora",
      email: "Dora@gmail.com",
      img: "https://i.pravatar.cc/150?img=7",
      active: false,
    },
    {
      id: 8,
      name: "Jessika",
      email: "Jessika@gmail.com",
      img: "https://i.pravatar.cc/150?img=8",
      active: false,
    },
    {
      id: 9,
      name: "Estella",
      email: "Estella@gmail.com",
      img: "https://i.pravatar.cc/150?img=9",
      active: false,
    },
    {
      id: 10,
      name: "Cholimate",
      email: "Cholimate@gmail.com",
      img: "https://i.pravatar.cc/150?img=10",
      active: false,
    },
    {
      id: 11,
      name: "Mayonelisa",
      email: "Mayonelisa@gmail.com",
      img: "https://i.pravatar.cc/150?img=11",
      active: false,
    },
    {
      id: 12,
      name: "ChinsuBanana",
      email: "ChinsuBanana@gmail.com",
      img: "https://i.pravatar.cc/150?img=12",
      active: false,
    },
  ],
  userActive: {
    id: 1,
    name: "Chelsie",
    email: "Chelsie@gmail.com",
    img: "https://i.pravatar.cc/150?img=1",
    active: true,
  },
};

const userManagementReducer = createSlice({
  name: "userManagementReducer",
  initialState,
  reducers: {
    changeUserActiveAction: (state, { payload }) => {
      const newListUser = state.listUser.map((item) => {
        return { ...item, active: false };
      });
      let index = newListUser.findIndex((item) => item.id === payload);
      if (index !== -1) {
        newListUser[index].active = true;
        state.userActive = newListUser[index];
      }
      state.listUser = newListUser;
    },
  },
});

export const { changeUserActiveAction } = userManagementReducer.actions;

export default userManagementReducer.reducer;
